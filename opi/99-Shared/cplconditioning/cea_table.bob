<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>CEA table</name>
  <width>1210</width>
  <height>730</height>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <x>10</x>
    <y>60</y>
    <width>1190</width>
    <height>670</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="table" version="2.0.0">
    <name>Table</name>
    <x>20</x>
    <y>70</y>
    <width>620</width>
    <height>650</height>
    <columns>
      <column>
        <name>Column1</name>
        <width>120</width>
        <editable>false</editable>
      </column>
    </columns>
    <scripts>
      <script file="EmbeddedPy" check_connections="false">
        <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil,ScriptUtil
from org.csstudio.display.builder.model.properties import WidgetColor

# Configure the table widget
# 1.Headers
# 2.ColumnWidth 
TableHeaders = ['Steps\n\n', 'RF Width [ms]\n', 'Rate [Hz]\n', 'Pmin [kw]\n', 'Pmax [kw]\n']
widget.setHeaders(TableHeaders)
for i in range(0,len(TableHeaders)):
	widget.setColumnWidth(i,120)

# Create matrix (2-D array) of data
# 16 row
# 4 columns
width=[]
for i in PVUtil.getDoubleArray(pvs[2]):
	width.append(str(round(i,3)))
repRate=[]
for i in PVUtil.getDoubleArray(pvs[3]):
	repRate.append(str(i))
minPwr=[]
for i in PVUtil.getDoubleArray(pvs[4]):
	minPwr.append(str(i))
maxPwr=[]
for i in PVUtil.getDoubleArray(pvs[5]):
	maxPwr.append(str(i))

# Fill in the table with array data
data=[]
for i in range(0,len(width)):
	dataset=[PVUtil.getStringArray(pvs[1])[i],width[i],repRate[i],minPwr[i],maxPwr[i]]
	data.append(dataset)
widget.setValue(data)

# General background white colour
for i in range(0,len(width)):
	for j in range(0,len(TableHeaders)):
		widget.setCellColor(i,j,WidgetColor(255,255,255))

# Selected item background green colour
index=int(PVUtil.getDouble(pvs[0]))
for i in range(0,len(TableHeaders)):
	widget.setCellColor(index,i,WidgetColor(0,125,0))
]]></text>
        <pv_name>$(P)$(R)SeqPulWidthSelRead.INDX</pv_name>
        <pv_name>$(P)$(R)SeqStatusSel</pv_name>
        <pv_name>$(P)$(R)SeqPulWidthSel</pv_name>
        <pv_name>$(P)$(R)SeqRepRateSel</pv_name>
        <pv_name>$(P)$(R)SeqPminSel</pv_name>
        <pv_name>$(P)$(R)SeqPmaxSel</pv_name>
      </script>
    </scripts>
    <editable>false</editable>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Polyline</name>
    <x>720</x>
    <y>130</y>
    <width>1</width>
    <height>80</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="80.0">
      </point>
    </points>
    <line_width>5</line_width>
    <arrows>2</arrows>
    <rules>
      <rule name="visible" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>$(P)$(R)CEATableConfig</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="group" version="2.0.0">
    <name>CEA Table Configuration</name>
    <x>650</x>
    <y>250</y>
    <width>540</width>
    <height>470</height>
    <style>1</style>
    <rules>
      <rule name="Visible" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <pv_name>$(P)$(R)CEATableConfig</pv_name>
      </rule>
    </rules>
    <widget type="array" version="2.0.0">
      <name>Array #01</name>
      <pv_name>$(P)$(R)SeqPulWidth-SP</pv_name>
      <x>120</x>
      <y>20</y>
      <height>320</height>
      <widget type="textentry" version="3.0.0">
        <name>Array #01 -textentry-0</name>
        <width>80</width>
        <horizontal_alignment>1</horizontal_alignment>
      </widget>
    </widget>
    <widget type="array" version="2.0.0">
      <name>Array #02</name>
      <pv_name>$(P)$(R)SeqRepRate-SP</pv_name>
      <x>220</x>
      <y>20</y>
      <height>320</height>
      <widget type="textentry" version="3.0.0">
        <name>Array #02 -textentry-0</name>
        <width>80</width>
        <horizontal_alignment>1</horizontal_alignment>
      </widget>
    </widget>
    <widget type="array" version="2.0.0">
      <name>Array #03</name>
      <pv_name>$(P)$(R)SeqPmin-SP</pv_name>
      <x>320</x>
      <y>20</y>
      <height>320</height>
      <widget type="textentry" version="3.0.0">
        <name>Array #03 -textentry-0</name>
        <width>80</width>
        <horizontal_alignment>1</horizontal_alignment>
      </widget>
    </widget>
    <widget type="array" version="2.0.0">
      <name>Array #04</name>
      <pv_name>$(P)$(R)SeqPmax-SP</pv_name>
      <x>420</x>
      <y>20</y>
      <width>99</width>
      <height>320</height>
      <widget type="textentry" version="3.0.0">
        <name>Array #04 -textentry-0</name>
        <width>79</width>
        <horizontal_alignment>1</horizontal_alignment>
      </widget>
    </widget>
    <widget type="array" version="2.0.0">
      <name>Array #05</name>
      <pv_name>$(P)$(R)SeqStatus-SP</pv_name>
      <x>20</x>
      <y>20</y>
      <height>320</height>
      <widget type="textentry" version="3.0.0">
        <name>Array #05 -textentry-0</name>
        <width>80</width>
        <horizontal_alignment>1</horizontal_alignment>
      </widget>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>BGGrey01-background #11</name>
      <x>20</x>
      <y>360</y>
      <width>500</width>
      <height>70</height>
      <line_width>1</line_width>
      <line_color>
        <color red="175" green="175" blue="175">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>3</corner_width>
      <corner_height>3</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label</name>
      <text>Start:</text>
      <x>30</x>
      <y>380</y>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label</name>
      <text>End:</text>
      <x>289</x>
      <y>380</y>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>Combo Box</name>
      <pv_name>$(P)$(R)SeqStart</pv_name>
      <x>130</x>
      <y>380</y>
      <width>120</width>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>Combo Box</name>
      <pv_name>$(P)$(R)SeqEnd</pv_name>
      <x>389</x>
      <y>380</y>
      <width>120</width>
    </widget>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>loc://ExpectedTime</pv_name>
    <x>980</x>
    <y>80</y>
    <width>200</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="20.0">
      </font>
    </font>
    <format>7</format>
    <show_units>false</show_units>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <scripts>
      <script file="EmbeddedPy" check_connections="false">
        <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import datetime

# Get minimum and maximum power array
Pmin = []
Pmax = []
for i in PVUtil.getDoubleArray(pvs[0]):
	Pmin.append(i)
for j in PVUtil.getDoubleArray(pvs[1]):
	Pmax.append(j)

# Get step forward, step time and soak time 
Pstepfwd = PVUtil.getDouble(pvs[2])
steptime = PVUtil.getInt(pvs[3])
soaktime = PVUtil.getDouble(pvs[4])

# expected time calculation
sum = 0
for k in range(0,len(Pmin),1):
	sum = sum+((Pmax[k]-Pmin[k])*steptime/Pstepfwd+soaktime)

# Write value to local PV
pvs[5].setValue(str(datetime.timedelta(seconds=sum)))	]]></text>
        <pv_name>$(P)$(R)SeqPminSel</pv_name>
        <pv_name>$(P)$(R)SeqPmaxSel</pv_name>
        <pv_name>$(P)$(R)StepForward</pv_name>
        <pv_name>$(P)$(R)StepTime</pv_name>
        <pv_name>$(P)$(R)PlateauTime</pv_name>
        <pv_name trigger="false">loc://ExpectedTime</pv_name>
      </script>
    </scripts>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1210</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>CEA Table Configuration</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>400</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Expected Time:</text>
    <x>841</x>
    <y>80</y>
    <width>140</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="bool_button" version="2.0.0">
    <name>Boolean Button</name>
    <pv_name>$(P)$(R)CEATableConfig</pv_name>
    <x>650</x>
    <y>80</y>
    <width>150</width>
    <labels_from_pv>true</labels_from_pv>
    <rules>
      <rule name="Enabled" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>false</value>
        </exp>
        <pv_name>$(P)$(R)Run</pv_name>
      </rule>
    </rules>
  </widget>
</display>

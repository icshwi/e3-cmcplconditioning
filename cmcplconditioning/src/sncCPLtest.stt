/* TS2 Coupler Conditioning SNL Program - Test Version */

/* Author: Gabriel Fedel */
/* Email:  gabriel.fedel@ess.eu */
/* Author: muyuanwang */
/* Email:  MuYuan.Wang@ess.eu */

/*=================== Program ======================*/
program sncConditioningTest
/* %% Macros:
    P: 
    R:
    LLRF: LLRF System controls the field of cavities amplitude and phase
    PI_TYPE: LLRF System output mode
*/

/*============= Escape to C Language ===============*/
%%#include <string.h>
%%#include <time.h>
%%#include <stdlib.h>
%%#include <stdio.h>
%%#include <unistd.h>

/*=================== Options ======================*/
option +r;
option +s;
option -c;

/*================== Declarations ==================*/
/* Running Mode */
int mode;
assign mode to "{P}{R}RunMode";
monitor mode;

/* Start. Stop. */
int startButton;
assign startButton to "{P}{R}Run";
monitor startButton;

/* Output Locker */
int locker;
assign locker to "{P}{R}Lock";
monitor locker;

/* Infinite Running Button */
int neverStop;
assign neverStop to "{P}{R}Inf";
monitor neverStop;

/* Keep Power Level */
int keepButton;
assign keepButton to "{P}{R}KeepPwrLevel";
monitor keepButton;

/* Pause. Restart Cycle */
int pauseButton;
assign pauseButton to "{P}{R}PauseCycleRestart";
monitor pauseButton;

/* Minimum Power */
float Pmin;
assign Pmin to "{P}{R}Pmin";

/* Maximum Power */
float Pmax;
assign Pmax to "{P}{R}Pmax";

/* Power Floor */
float PowFloor;
assign PowFloor to "{P}{R}PwrBase";
monitor PowFloor;

/* Power Up */
float StepUp;
assign StepUp to "{P}{R}StepForward";
monitor StepUp;

/* Power Down */
float StepDown;
assign StepDown to "{P}{R}StepBack";
monitor StepDown;

/* Plateau Time */
float pulsePlateau;
assign pulsePlateau to "{P}{R}PlateauTime";
monitor pulsePlateau;

/* RF Pulse width Step */
float StepWidth;
assign StepWidth to "{P}{R}PulseWidthStep";
monitor StepWidth;

/* Minimum RF Pulse width */
float widthmin;
assign widthmin to "{P}{R}PulseWidthMinimum";
monitor widthmin;

/* Maximum RF Pulse width */
float widthmax;
assign widthmax to "{P}{R}PulseWidthMaximum";
monitor widthmax;

/* Count Step Pulse */
float nbPulsestep;
assign nbPulsestep to "{P}{R}CountPulseStep";
monitor nbPulsestep;

/* Step time */
int steptime;
assign steptime to "{P}{R}StepTime";
monitor steptime;

/* Timer */
int timer;
assign timer to "{P}{R}Timer";

/* Interlock */
int Criticalfault;
assign Criticalfault to "{P}{R}CriticalFault";
monitor Criticalfault;

/* Software Interlock Fault */
int Softfault;
assign Softfault to "{P}{R}SoftFault";
monitor Softfault;

/* Power Fault */
int Pwrfault;
assign Pwrfault to "{P}{R}PwrFault";
monitor Pwrfault;

/* Duty cycle */
float RepFreq;
assign RepFreq to "{EVR}MixFreq";

/* RF pulse width */
float width;
assign width to "{EVR}RFSyncWdt-SP";

/* Count pulse */
int countPulse;
assign countPulse to "{EVR}Cycle-Cnt";
monitor countPulse;

/*=========== Feed Forward Table Output Power ===========*/
/* Number of Ramp waveform */
int RampNumber;
assign RampNumber to "{LLRF}{PI_TYPE}TblRampSmpNm";

/* Start of Magnitude Array */
float RampStrMag;
assign RampStrMag to "{LLRF}{PI_TYPE}TblRampStr-Mag";

/* End of Magnitude Array */
float RampEndMag;
assign RampEndMag to "{LLRF}{PI_TYPE}TblRampEnd-Mag";

/* Start of Angle Array */
float RampStrAng;
assign RampStrAng to "{LLRF}{PI_TYPE}TblRampStr-Ang";

/* End of Angle Array */
float RampEndAng;
assign RampEndAng to "{LLRF}{PI_TYPE}TblRampEnd-Ang";

/*==================== Sequence Settings ====================*/
/* Sequence Index */
int index[5];
assign index[0] to "{P}{R}SeqStatusSelRead.INDX";
assign index[1] to "{P}{R}SeqPulWidthSelRead.INDX";
assign index[2] to "{P}{R}SeqRepRateSelRead.INDX";
assign index[3] to "{P}{R}SeqPminSelRead.INDX";
assign index[4] to "{P}{R}SeqPmaxSelRead.INDX";

/* Sequence Read */
float Read[5];
assign Read[0] to "{P}{R}SeqStatusSelRead";
assign Read[1] to "{P}{R}SeqPulWidthSelRead";
assign Read[2] to "{P}{R}SeqRepRateSelRead";
assign Read[3] to "{P}{R}SeqPminSelRead";
assign Read[4] to "{P}{R}SeqPmaxSelRead";

/* Sequence Length */
int length;
assign length to "{P}{R}SeqLength";
monitor length;

/*==================== Variable Settings ====================*/
/* Critical/Vacuum fault:
    0: Disable;
    1: Enable;
*/
int cf=1; // critical fault (general state);
int flag=1; // vacuum fault (general state);
int keep_vac=1; // vacuum fault (keep state);
int keep_cf=1; // critical fault (keep state);

/* General variable settings:
    0: Default state;
    1: Trigger state;
*/
int start=1; // start operation;
int os=0; // operation stop;
int floor=0; // power floor;
int soak=0; // plateau;
int firstcycle=0; // first cycle run;
int cnt=0; // cycle counter (Debug);
int seq_cnt=0; // sequence counter (Sequence);
int seq_end=0; // End the sequence;
int seq_timer=0; // timer (Sequence);

/* Value */
int i=0; // iteration;
int countPulsestep=0; // pulse count step;
float keep_value=0; // keep state value;

/*====================== State Sets ======================*/

ss ss2{
    /*========== init ==========*/
    state init
    {
        when (os==1 && startButton==0)
        {
            printf("Operation Stop\n");
            sleep(1);
            os=0;
        } state init

        when (Criticalfault==0 && startButton==1)
        {      
            printf("Critical Fault\n");
            sleep(1);
        } state init

        when (locker==1 && startButton==1)
        {
            printf("Thread stuck cause the other coupler conditioning sequencer is running!\n");
            startButton=0;
            pvPut(startButton,SYNC);
        } state init

        /* Power Initialization... */
        when (startButton==1 && locker==0 && Criticalfault==1 && Pwrfault==0)
        {
            if (cf==1)
            {
                RampNumber=5;
                pvPut(RampNumber,SYNC);
                RampStrAng=0;
                pvPut(RampStrAng,SYNC);
                RampEndAng=0;
                pvPut(RampEndAng,SYNC);
                RampStrMag=0;
                pvPut(RampStrMag,SYNC);
                RampEndMag=10;
                pvPut(RampEndMag,SYNC);
                printf("LLRF Output Power Initialization...\n");
            }
            if (cf==0)
            {
                cf=1;
                RampEndMag=10;
                pvPut(RampEndMag,SYNC);
                printf("LLRF Output Power Initialization (Interlock)...\n");
            }
        } state init

        when (startButton==1 && locker==0 && Criticalfault==1 && Pwrfault==1)
        {
            if (Softfault==0) {printf("Wait for vacuum to get better (init state)...\n"); sleep(1);}
        } state CycleControl
    }

    /*========== stop ==========*/
    state stop
    {
        when (os==1)
        {
            start=1;
            if (mode==0)
            {
                cnt=0;
                RampEndMag=Pmin;
                pvPut(RampEndMag,SYNC);
                width=widthmin;
                pvPut(width,SYNC);
            }
            if (mode==1)
            {
                seq_cnt=0;
                if (seq_end==1) {seq_end=0;}
                for (i=0; i<5; i++)
                {
                    index[i]=seq_cnt;         
                    pvPut(index[i],SYNC);
                    pvGet(Read[i]);
                }
                Pmin=Read[3];
                pvPut(Pmin,SYNC);               
                RampEndMag=Pmin;
                pvPut(RampEndMag,SYNC);
                Pmax=Read[4];
                pvPut(Pmax,SYNC);
                width=Read[1]*1000;
                pvPut(width,SYNC);
                RepFreq=Read[2];
                pvPut(RepFreq,SYNC);
            }   
        } state init
    }

    /*========== Pause ==========*/
    state pause
    {
        when (pauseButton==1 && startButton==1 && Criticalfault==1 && Softfault==1)
        {
            printf("Pause. Cycle Restart\n");
            sleep(1);
        } state pause

        when (pauseButton==0 && startButton==1 && Criticalfault==1 && Softfault==1)
        {printf("Go back to PowerControl state...\n");} state PowerControl

        when (pauseButton==1 && startButton==0 && Criticalfault==1)
        {
            os=1;
            pauseButton=0;
            pvPut(pauseButton,SYNC);
        } state stop

        when (Criticalfault==0)
        {cf=0;} state init

        when (Softfault==0 && Criticalfault==1)
        {
            printf("Wait for vacuum to get better (pause state)...\n");
            sleep(1);
        } state pause
    }
    
    /*========== Keep ==========*/
    state keep
    {
        when (keepButton==1 && startButton==1 && Criticalfault==1 && Softfault==1)
        {
            printf("Keep Power\n");
            sleep(1);
            if (seq_timer==1) {timer+=1; pvPut(timer,SYNC);}
        } state keep

        when (keepButton==0 && startButton==1 && Criticalfault==1 && Softfault==1)
        {printf("Go back to PowerControl state...\n");} state PowerControl

        when (keepButton==1 && startButton==0 && Criticalfault==1)
        {
            os=1;
            keepButton=0;
            pvPut(keepButton,SYNC);
        } state stop

        when (Criticalfault==0)
        {
            cf=0;
            keep_cf=0;
            pvGet(RampEndMag,SYNC);
            keep_value=RampEndMag;
        } state init

        when (Softfault==0 && Criticalfault==1)
        {            
            keep_vac=0;
            pvGet(RampEndMag,SYNC);
            keep_value=RampEndMag;
        } state PowerControl
    }
    
    /*========== CycleControl ==========*/
    state CycleControl
    {
        /* Cycle finished */
        when ((mode==0 && cnt==2) || (mode==1 && seq_end==1))
        {
            start=1;
            printf("End of Sequence.\n");
            RampEndMag=10;
            pvPut(RampEndMag,SYNC);
            RepFreq=1;
            pvPut(RepFreq,SYNC);
            width=50;
            pvPut(width,SYNC);
            startButton=0;
            pvPut(startButton,SYNC);
            if (mode==0) {cnt=0;}
            if (mode==1)
            {
                seq_end=0;
                seq_cnt=0;
                for (i=0; i<5; i++)
                {
                    index[i]=seq_cnt;
                    pvPut(index[i],SYNC);
                }
            }
            if (seq_timer==1) {seq_timer=0;}
        } state init

        /* Cycle Start */
        when (Criticalfault==1 && Softfault==1)
        {
            firstcycle=1;
            floor=0;
            soak=0;
            os=0;
            if (flag==0) {flag=1;}
            if (keep_vac==0) {keep_vac=1;}
            if (mode==0)
            {
                pvGet(Pmin);
                RampEndMag=Pmin;
                pvPut(RampEndMag,SYNC);
                pvGet(Pmax);
                pvGet(RepFreq);
                /* First Cycle */
                if (start==1)
                {
                    start=0;            
                    countPulsestep=0;
                    width=widthmin;
                    pvPut(width,SYNC);        
                }
                else {pvPut(width,SYNC);}
            }
            if (mode==1)
            {
                for (i=0; i<5; i++)
                {
                    index[i]=seq_cnt;          
                    pvPut(index[i],SYNC);
                    pvGet(Read[i]);
                }
                Pmin=Read[3];
                pvPut(Pmin,SYNC);               
                RampEndMag=Pmin;
                pvPut(RampEndMag,SYNC);
                Pmax=Read[4];
                pvPut(Pmax,SYNC);
                width=Read[1]*1000;
                pvPut(width,SYNC);
                RepFreq=Read[2];
                pvPut(RepFreq,SYNC);        
                if (start==1)
                {
                    start=0;
                    countPulsestep=0;
                    /* Initialize timer in sequence mode */
                    if (seq_timer==0) {seq_timer=1; timer=0; pvPut(timer,SYNC);}
                }
            }
            printf("Starting the Sequence: %.3fms\n", width/1000);
        } state PowerControl
    }

    /*========== nominal ==========*/
    state nominal
    {
        when (startButton==0 && Criticalfault==1)
        {
            os=1;
            if (keepButton==1) {keepButton=0; pvPut(keepButton,SYNC);}
            if (pauseButton==1) {pauseButton=0; pvPut(pauseButton,SYNC);}
        } state stop

        when (Criticalfault==0)
        {cf=0;} state init

        /* plateau */
        when (soak==1 && Criticalfault==1 && Softfault==1)
        {} state PowerControl    
    
        /* Software vacuum interlock happened */
        when (Softfault==0 && flag==1 && Criticalfault==1)
        {} state PowerControl
    
        /* Software vacuum interlock reset */
        when (Softfault==1 && flag==0 && Criticalfault==1)
        {} state PowerControl

        when (countPulse-countPulsestep>=nbPulsestep && Criticalfault==1)
        {      
            if (firstcycle==1) {firstcycle=0;}
            if (floor==0) {if (RampEndMag>=PowFloor) {floor=1;}}
            if (seq_timer==1) {timer+=steptime; pvPut(timer,SYNC);}
            printf("%d, %d, %d\n",countPulse, countPulsestep, (int)(nbPulsestep+0.5));
        } state PowerControl
    }

    /*========== PowerControl ==========*/
    state PowerControl
    {
        /* keep */
        when (keepButton==1 && pauseButton==0 && Criticalfault==1 && Softfault==1 && keep_vac==1 && keep_cf==1)
        {printf("Go to keep state\n");} state keep

        /* Go back to keep */
        when (keepButton==1 && pauseButton==0 && keep_value==RampEndMag && Criticalfault==1 && Softfault==1)
        {
            keep_vac=1;
            keep_cf=1;
            //if (keep_cf==0) {keep_cf=1;}
            printf("Go back to keep state\n");
        } state keep

        /* pause */
        when (pauseButton==1 && keepButton==0 && Criticalfault==1 && Softfault==1)
        {
            RampEndMag=Pmin;
            pvPut(RampEndMag,SYNC);
        } state pause

        when (startButton==0 && Criticalfault==1)
        {
            os=1;
            if (keepButton==1) {keepButton=0; pvPut(keepButton,SYNC);}
            if (pauseButton==1) {pauseButton=0; pvPut(pauseButton,SYNC);}
        } state stop

        when (Criticalfault==0)
        {cf=0;} state init

        /* plateau */
        when (RampEndMag>=Pmax && Softfault==1 && Criticalfault==1)
        {                     
            if (soak==1) {printf("Reached Plateau\n"); countPulsestep=countPulse;}
        } state plateau

        /* Power up */
        when (Softfault==1 && Criticalfault==1)
        {
            if (flag==0) {flag=1;}
            countPulsestep=countPulse;
            if (firstcycle==1) {}
            else
            {
                RampEndMag=RampEndMag+StepUp;
                if (RampEndMag>=Pmax)
                {
                    RampEndMag=Pmax;
                    soak=1;
                }
                pvPut(RampEndMag,SYNC);
                printf("%.3fms, %.1fHz, %.1fkw\n",width/1000, RepFreq, RampEndMag);
            }
        } state nominal

        /* Power down */
        when (Softfault==0 && Criticalfault==1)
        {
            if (flag==1) {flag=0;}
            if (soak==1) {soak=0;}
            if (firstcycle==0) {firstcycle=1;}
            countPulsestep=countPulse;
            RampEndMag=RampEndMag-StepDown;
            if (floor==0 && Pmin>=RampEndMag)
            {
                RampEndMag=Pmin;
                pvPut(RampEndMag,SYNC);
                printf("Vacuum software interlock, keep minimum power...\n");
            }
            if (floor==1 && PowFloor>=RampEndMag)
            {
                RampEndMag=PowFloor;
                pvPut(RampEndMag,SYNC);
                printf("Vacuum software interlock, keep floor power...\n");
            }
            else
            {
                pvPut(RampEndMag,SYNC);
                printf("Vacuum software interlock, power decreasing...\n");
            }
        } state nominal    
    }

    /*========== plateau ==========*/    
    state plateau
    {
        /* Keep */
        when (pauseButton==0 && keepButton==1 && Criticalfault==1 && Softfault==1)
        {printf("Go to keep state\n");} state keep

        /* Pause */
        when (keepButton==1 && pauseButton==1 && Criticalfault==1 && Softfault==1)
        {
            RampEndMag=Pmin;
            pvPut(RampEndMag,SYNC);
            printf("Go to pause state\n");
        } state pause

        /* Stop */
        when (startButton==0 && Criticalfault==1)
        {
            os=1;
            if (keepButton==1) {keepButton=0; pvPut(keepButton,SYNC);}
            if (pauseButton==1) {pauseButton=0; pvPut(pauseButton,SYNC);}
        } state stop

        /* Critical Fault */
        when (Criticalfault==0)
        {cf=0;} state init

        /* Software Interlock */
        when (Criticalfault==1 && Softfault==0)
        {} state nominal

        when (delay(pulsePlateau))
        {
            if (seq_timer==1) {timer+=pulsePlateau; pvPut(timer,SYNC);}
            printf("%.3fms, %.1fHz Cycle Finished.\n\n", width/1000, RepFreq);
            if (mode==0)
            {
                /* Single RF pulse width */
                if (widthmin==widthmax)
                {
                    cnt=2;
                    if (cnt==2 && neverStop==1) {cnt=0; width=widthmin;}
                }
                else
                {
                    width=width+StepWidth;
                    if (width>=widthmax)
                    {
                        width=widthmax;
                        cnt++;
                        if (cnt==2 && neverStop==1) {cnt=0; width=widthmin;}
                    }
                }
            }
            if (mode==1)
            {
                seq_cnt++;
                if (seq_cnt>length-1) {seq_end=1;}
            }
        } state CycleControl
    }
}

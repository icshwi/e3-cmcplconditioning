# This should be a test or example startup script

require cmcplconditioning

# P: ex: MBL-010Crm:
# R: ex: EMR-Cav-010:
# DISDEV: ex: EMR-Cav-
# LLRFCOM: ex: MBL-010RFC:RFS-LLRF-
# FIMCOM: ex: MBL-010RFC:RFS-FIM-
# VACCOM: ex: MBL-010Crm:Vac-VGC-
# EPUCOM: ex: MBL-010RFC:RFS-EPR-110:
epicsEnvSet("P","Lab-010Crm:")
epicsEnvSet("R","EMR-Cav-010:")
epicsEnvSet("LLRFCOM","LAB-010RFC:RFS-LLRF-")
epicsEnvSet("LLRFDIGCOM","LAB-010RFC:RFS-DIG-")
epicsEnvSet("FIMCOM","LAB-010RFC:RFS-FIM-")
epicsEnvSet("EPUCOM","LAB-010RFC:RFS-EPR-")
epicsEnvSet("EVR","OCTOPUS-010:Ctrl-EVR-101:")
epicsEnvSet("PWRAMPFWDCH","AI7")


iocshLoad("$(cmcplconditioning_DIR)/cplcond_test.iocsh")
iocshLoad("$(cmcplconditioning_DIR)/cplcond.iocsh")
